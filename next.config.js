/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  pageExtensions: ["page.tsx", "page.ts", "page.jsx", "page.js"],
  webpack(config) {
    config.module.rules.push({
      test: /\.(ogg|mp3|wav)$/i,
      type: "asset/resource",
    });
    return config;
  },
  ...(process.env.NODE_ENV === "production" && process.env.PUBLIC_URL
    ? {
        assetPrefix: process.env.PUBLIC_URL,
      }
    : {}),
};

module.exports = nextConfig;
