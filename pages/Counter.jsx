import { Stack, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { useStateProvider } from "./StateProvider";

export default function Counter() {
  const {
    counter: [counter],
    bpmStates: [bpmStates],
    playing: [playing],
  } = useStateProvider();
  const [parentCounter, setParentCounter] = useState(null);
  useEffect(() => {
    if ([1, 2, 3, 4].includes(counter)) {
      setParentCounter(counter);
    }
  }, [counter]);
  return (
    <Stack direction="row" spacing={1}>
      {[1, 2, 3, 4]
        .map((num) => {
          return [
            <Typography variant={parentCounter === num ? "h4" : "h6"} key={num}>
              {num}
            </Typography>,
            ...{
              "1/4": [],
              "1/8": ["n"],
              "1/16": ["i", "n", "d"],
            }[bpmStates[playing].note].map((n) => (
              <Typography
                variant={
                  parentCounter === num && counter === n ? "h5" : "overline"
                }
                key={`${num}${n}`}
              >
                {n.toUpperCase()}
              </Typography>
            )),
          ];
        })
        .reduce((prev, curr) => [...prev, ...curr], [])}
    </Stack>
  );
}
