import PauseIcon from "@mui/icons-material/Pause";
import PlayArrowIcon from "@mui/icons-material/PlayArrow";
import {
  Box,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  IconButton,
} from "@mui/material";
import Typography from "@mui/material/Typography";

export default function PreviewCard({
  bpm,
  note,
  isPlaying,
  onPlayPause,
  onClick,
  sx,
  ...props
}) {
  return (
    <Card
      variant="outlined"
      {...props}
      sx={{ ...sx, display: "flex", overflow: "initial" }}
    >
      <CardActionArea onClick={onClick}>
        <CardContent sx={{ display: "flex", alignItems: "end", gap: 1 }}>
          <Typography variant="h3">{bpm}</Typography>
          <Box>
            <Typography mb={-1} variant="subtitle2">
              {
                {
                  "1/4": "♩",
                  "1/8": "♫",
                  "1/16": "♬",
                }[note]
              }
            </Typography>
            <Typography variant="overline" color="text.secondary">
              bpm
            </Typography>
          </Box>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <IconButton
          size="large"
          onClick={() => onPlayPause(isPlaying ? "pause" : "play")}
        >
          {isPlaying ? (
            <PauseIcon fontSize="large" />
          ) : (
            <PlayArrowIcon fontSize="large" />
          )}
        </IconButton>
      </CardActions>
    </Card>
  );
}
