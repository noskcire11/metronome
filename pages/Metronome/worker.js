import { clearDriftless, setDriftlessInterval } from "driftless";

let timerId = null;
let i = 0;
onmessage = ({ data }) => {
  switch (data.type) {
    case "start":
      work(data);
      timerId = setDriftlessInterval(
        () => work(data),
        1000 * ((60 * 4 * eval(data.note)) / data.bpm)
      );
      break;
    case "stop":
      clearDriftless(timerId);
      close();
      break;
  }
};
const work = ({ note }) => {
  if (i === 16) {
    i = 0;
  }
  switch (i) {
    case 0:
      postMessage(1);
      i = 0;
      break;
    case 4:
      postMessage(2);
      break;
    case 8:
      postMessage(3);
      break;
    case 12:
      postMessage(4);
      break;
    case 1:
    case 5:
    case 9:
    case 13:
      postMessage("i");
      break;
    case 2:
    case 6:
    case 10:
    case 14:
      postMessage("n");
      break;
    case 3:
    case 7:
    case 11:
    case 15:
      postMessage("d");
      break;
  }
  switch (note) {
    case "1/4":
      i += 4;
      break;
    case "1/8":
      i += 2;
      break;
    case "1/16":
      i++;
      break;
  }
};
