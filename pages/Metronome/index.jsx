import { useStateProvider } from "pages/StateProvider";
import { useEffect } from "react";
import click1 from "./click1.wav";
import click2 from "./click2.wav";

export default function Metronome({ bpm, note }) {
  const {
    counter: [, setCounter],
  } = useStateProvider();
  useEffect(() => {
    const worker = new Worker(new URL("./worker", import.meta.url));
    worker.onmessage = ({ data }) => {
      setCounter(data);
      switch (data) {
        case 1:
        case 2:
        case 3:
        case 4:
          new Audio(click1).play();
          break;

        case "i":
        case "n":
        case "d":
          new Audio(click2).play();
          break;
      }
    };
    worker.postMessage({ type: "start", bpm, note });
    return () => {
      worker.postMessage({ type: "stop" });
    };
  }, [bpm, note, setCounter]);
}
