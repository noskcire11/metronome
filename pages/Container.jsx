import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import {
  Box,
  Button,
  Chip,
  Divider,
  Grid,
  Paper,
  useMediaQuery,
} from "@mui/material";
import { useCallback, useState } from "react";
import Editor from "./Editor";
import Metronome from "./Metronome";
import PreviewCard from "./PreviewCard";
import { useStateProvider } from "./StateProvider";

export default function Container() {
  const {
    bpmStates: [bpmStates, setBpmStates],
    playing: [playing, setPlaying],
  } = useStateProvider();
  const [selectedIndex, setSelectedIndex] = useState(null);
  const addNewBpm = useCallback(
    () => setBpmStates((bpms) => [...bpms, { bpm: 100, note: "1/4" }]),
    [setBpmStates]
  );
  const isMediaMd = useMediaQuery((theme) => theme.breakpoints.down("md"));
  return (
    <Box
      p={{ md: 10 }}
      height="100%"
      background="linear-gradient(rgb(255, 38, 142) 0%, rgb(255, 105, 79) 100%)"
    >
      {playing != null && (
        <Metronome
          bpm={bpmStates[playing].bpm}
          note={bpmStates[playing].note}
        />
      )}
      <Grid container component={Paper} height="100%">
        {isMediaMd && selectedIndex != null ? (
          <Grid item xs={12}>
            <Button fullWidth onClick={() => setSelectedIndex(null)}>
              <ArrowBackIcon />
            </Button>
          </Grid>
        ) : (
          <>
            <Grid item xs={12} md={5} lg={3} height="100%">
              <Grid
                container
                spacing={2}
                height="100%"
                overflow="scroll"
                alignContent="start"
                p={2}
              >
                {bpmStates.map(({ bpm, note }, index) => (
                  <Grid key={index} item xs={12}>
                    <PreviewCard
                      bpm={bpm}
                      note={note}
                      isPlaying={index === playing}
                      onPlayPause={() =>
                        playing === index ? setPlaying(null) : setPlaying(index)
                      }
                      onClick={() => setSelectedIndex(index)}
                      {...(index === selectedIndex
                        ? {
                            variant: "elevation",
                            elevation: 3,
                          }
                        : {})}
                    />
                  </Grid>
                ))}
                <Grid item xs={12}>
                  <Divider
                    sx={{
                      opacity: 0.2,
                      transition: "0.35s",
                      "&:hover": {
                        opacity: 0.75,
                      },
                    }}
                  >
                    <Chip onClick={addNewBpm} label="+" variant="outlined" />
                  </Divider>
                </Grid>
              </Grid>
            </Grid>
            <Divider orientation="vertical" sx={{ marginRight: "-1px" }} />
          </>
        )}
        {selectedIndex != null && (
          <Grid item xs={12} md={7} lg={9} height="100%">
            <Editor index={selectedIndex} />
          </Grid>
        )}
      </Grid>
    </Box>
  );
}
