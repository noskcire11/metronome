import { useEffect, useState } from "react";
import Container from "./Container";
import { StateProvider } from "./StateProvider";

export default function Index() {
  const [domLoaded, setDomLoaded] = useState(false);

  useEffect(() => {
    setDomLoaded(true);
  }, []);
  return (
    domLoaded && (
      <StateProvider>
        <Container />
      </StateProvider>
    )
  );
}
