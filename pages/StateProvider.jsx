import { createContext, useContext, useMemo, useState } from "react";
import createPersistedState from "use-persisted-state";

const StateContext = createContext();

const useBpmStates = createPersistedState("bpm-states");

export function StateProvider({ children }) {
  const [bpmStates, setBpmStates] = useBpmStates([
    {
      bpm: 100,
      note: "1/4",
    },
  ]);
  const [playing, setPlaying] = useState(null);
  const [counter, setCounter] = useState(null);
  const value = useMemo(
    () => ({
      bpmStates: [bpmStates, setBpmStates],
      playing: [playing, setPlaying],
      counter: [counter, setCounter],
    }),
    [bpmStates, counter, playing, setBpmStates]
  );
  return (
    <StateContext.Provider value={value}>{children}</StateContext.Provider>
  );
}

export function useStateProvider() {
  return useContext(StateContext);
}
