import AddIcon from "@mui/icons-material/Add";
import PauseIcon from "@mui/icons-material/Pause";
import PlayArrowIcon from "@mui/icons-material/PlayArrow";
import RemoveIcon from "@mui/icons-material/Remove";
import {
  Box,
  Button,
  Grid,
  IconButton,
  Slider,
  Stack,
  Typography,
  useMediaQuery,
} from "@mui/material";
import { useCallback, useMemo } from "react";
import Counter from "./Counter";
import { useStateProvider } from "./StateProvider";

export default function Editor({ index }) {
  const {
    bpmStates: [bpmStates, setBpmStates],
    playing: [playing, setPlaying],
  } = useStateProvider();
  const bpmState = useMemo(() => bpmStates[index], [bpmStates, index]);
  const setBpmState = useCallback(
    (value) =>
      setBpmStates((bpmStates) => [
        ...bpmStates.slice(0, index),
        typeof value === "function" ? value(bpmStates[index]) : value,
        ...bpmStates.slice(index + 1),
      ]),
    [index, setBpmStates]
  );
  const isMediaMd = useMediaQuery((theme) => theme.breakpoints.down("md"));
  return (
    <Box
      display="flex"
      flexDirection="column"
      alignItems="center"
      justifyContent="center"
      height="100%"
    >
      {index === playing && <Counter />}
      <Grid
        container
        direction="column"
        p={2}
        width={isMediaMd ? "80%" : "60%"}
      >
        <Grid item>
          <Box
            display="flex"
            alignItems="end"
            gap={{ xs: 1, md: 3 }}
            justifyContent="center"
          >
            <Typography
              variant="h3"
              fontSize={{
                md: "5em",
                lg: "10em",
              }}
            >
              {bpmState.bpm}
            </Typography>
            <Box>
              <Typography
                variant="subtitle2"
                mb={{ xs: -1, md: -2, lg: -3 }}
                fontSize={{
                  md: "1.25em",
                  lg: "2.5em",
                }}
                component={Button}
                display="block"
                color="text.primary"
                minWidth="unset"
                pb={0.3}
                pt={0.3}
                onClick={() =>
                  setBpmState(({ bpm, note }) => ({
                    bpm,
                    note: { "1/4": "1/8", "1/8": "1/16", "1/16": "1/4" }[note],
                  }))
                }
              >
                {
                  {
                    "1/4": "♩",
                    "1/8": "♫",
                    "1/16": "♬",
                  }[bpmState.note]
                }
              </Typography>
              <Typography
                variant="overline"
                color="text.secondary"
                fontSize={{
                  md: "1.25em",
                  lg: "2.5em",
                }}
              >
                bpm
              </Typography>
            </Box>
          </Box>
        </Grid>
        <Grid item>
          <Stack spacing={1} direction="row" sx={{ mb: 1 }} alignItems="center">
            <IconButton
              onClick={() =>
                setBpmState(({ bpm, note }) => ({
                  bpm: bpm - 1,
                  note,
                }))
              }
              size={isMediaMd ? "small" : "large"}
            >
              <RemoveIcon fontSize={isMediaMd ? "small" : "large"} />
            </IconButton>
            <Slider
              color="secondary"
              min={20}
              max={500}
              value={bpmState.bpm}
              onChange={(e, value) =>
                setBpmState(({ note }) => ({
                  bpm: value,
                  note,
                }))
              }
              {...(isMediaMd
                ? { size: "small" }
                : {
                    marks: [
                      { value: 20, label: "20" },
                      { value: 60, label: "60" },
                      { value: 100, label: "100" },
                      { value: 280, label: "280" },
                      { value: 500, label: "500" },
                    ],
                  })}
            />
            <IconButton
              onClick={() =>
                setBpmState(({ bpm, note }) => ({
                  bpm: bpm + 1,
                  note,
                }))
              }
              size={isMediaMd ? "small" : "large"}
            >
              <AddIcon fontSize={isMediaMd ? "small" : "large"} />
            </IconButton>
          </Stack>
        </Grid>
        <Grid item textAlign="center" {...(isMediaMd ? {} : { mt: 5 })}>
          <IconButton
            onClick={() =>
              playing === index ? setPlaying(null) : setPlaying(index)
            }
          >
            {playing === index ? (
              <PauseIcon sx={{ fontSize: { xs: "1.75em", md: "5em" } }} />
            ) : (
              <PlayArrowIcon sx={{ fontSize: { xs: "1.75em", md: "5em" } }} />
            )}
          </IconButton>
        </Grid>
      </Grid>
    </Box>
  );
}
