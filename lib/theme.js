import { red } from "@mui/material/colors";
import { createTheme } from "@mui/material/styles";

// Create a theme instance.
const theme = createTheme({
  components: {
    MuiCssBaseline: {
      styleOverrides: `
        html,
        body,
        div#__next {
          height: 100%;
        }
      `,
    },
  },
  palette: {
    mode: "dark",
    error: {
      main: red.A400,
    },
  },
});

export default theme;
